#!/bin/bash -ex

#Simple lookup - only 1 matching verse
bible -f '??girl' | grep 'Jl3:3'

#Simple lookup - 1840 matches, 2 header lines
diff -u <( bible -f '??hath' | wc -l ) <(echo 1842)

#Check text of a verse is correct
bible -f Jn11:35 | grep -F "John11:35 Jesus wept."

#Check search combination (cumbersome on the CLI)
diff -u <( bible -f ??beginning "?a end" "?a year" | tail -1 ) <( echo "Dt11:12" )

#Check output matches entire rawtext file (less header line)
diff -u <( bible -f 'gen1:1-rev99:99' ) <( tail -n +2 bible.rawtext )
